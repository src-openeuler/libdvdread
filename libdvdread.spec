Name:           libdvdread
Version:        6.1.3
Release:        1
Summary:        Library to access DVD disks
License:        GPLv2+
URL:            http://dvdnav.mplayerhq.hu/
Source0:        https://download.videolan.org/pub/videolan/libdvdread/%{version}/libdvdread-%{version}.tar.bz2
Source1:        https://download.videolan.org/pub/videolan/libdvdread/%{version}/libdvdread-%{version}.tar.bz2.asc
Source2:        https://download.videolan.org/pub/keys/7180713BE58D1ADC.asc

BuildRequires:  gcc gnupg2
Provides:       bundled(md5-gcc)

%description
Libdvdread provides a simple foundation for reading DVD-Video images.
It offers several DVD applications and allows application designers
to access some of the more advanced features of the DVD format.

%package        devel
Summary:        Development files for libdvdread
Requires:       %{name} = %{version}-%{release} pkgconfig

%description    devel
Libdvdread-devel contains development files for libdvdread.

%prep
gpg2 --import --import-options import-export,import-minimal %{S:2} > ./gpg-keyring.gpg
gpgv2 --keyring ./gpg-keyring.gpg %{S:1} %{S:0}
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
%make_build V=1

%install
%make_install
%delete_la

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%license COPYING
%doc AUTHORS NEWS README.md ChangeLog TODO
%{_libdir}/libdvdread.so.*
%exclude %{_pkgdocdir}/COPYING

%files devel
%{_includedir}/dvdread
%{_libdir}/libdvdread.so
%{_libdir}/pkgconfig/dvdread.pc

%changelog
* Fri Jul 29 2022 wenzhiwei <wenzhiwei@kylinos.cn> - 6.1.3-1
- Upgrade to 6.1.3

* Tue Feb 18 2020 zhusongbao <zhusongbao@huawei.com> 6.0.0-3
- Package init
